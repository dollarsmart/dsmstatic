<?php get_header(); ?>

    <div id="content">

        <?php if (have_posts()) : ?>

         <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
         <?php /* If this is a category archive */ if (is_category()) { ?>
		<h1 class="pagetitle">Archive for the &#8216;<?php echo single_cat_title(); ?>&#8217; Category</h1>

 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h1 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h1>

	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h1 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h1>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h1 class="pagetitle">Archive for <?php the_time('Y'); ?></h1>

	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h1 class="pagetitle">Author Archive</h2>

		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h1 class="pagetitle">Blog Archives</h1>

		<?php } ?>


		<?php while (have_posts()) : the_post(); ?>
		<div class="post">
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h3>
				<div class="postmetadata">
				<p class="postmetacomment"><?php comments_popup_link('<span class="commentcount">No</span> Comments', '<span class="commentcount">1</span> Comment', '<span class="commentcount">%</span> Comments'); ?></p>
	 			<p class="metadata">Posted in <?php the_category(', ') ?> <?php edit_post_link('Edit', '', ' | '); ?> by <?php the_author() ?> on <?php the_time('F jS, Y') ?> </p>
				</div>
				
				<div class="entry">
					<?php the_content() ?>
				</div>

			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div><?php next_posts_link('&lt; Previous') ?></div>
			<div><?php previous_posts_link('Next &gt;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>

	<?php endif; ?>

	</div>

    

<?php get_sidebar(); ?>
<?php get_footer(); ?>
