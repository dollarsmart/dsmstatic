<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/sidebar.php'); ?>
<div id="content" class="home">
		<div class="box-big right">
    	<h2>Watch Video</h2>
        <span class="center centertxt">
            <object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="player" width="400" height="315">
                <param name="movie" value="<?php bloginfo('template_directory'); ?>/flash/player.swf" />
                <param name="allowfullscreen" value="true" />
                <param name="allowscriptaccess" value="always" />
                <param name="flashvars" value="file=<?php bloginfo('template_directory'); ?>/flash/video.flv&image=<?php bloginfo('template_directory'); ?>/flash/preview.jpg" />
                <embed
                    type="application/x-shockwave-flash"
                    id="player2"
                    name="player2"
                    src="<?php bloginfo('template_directory'); ?>/flash/player.swf" 
                    width="400" 
                    height="315"
                    allowscriptaccess="always" 
                    allowfullscreen="true"
                    flashvars="file=<?php bloginfo('template_directory'); ?>/flash/video.flv&image=<?php bloginfo('template_directory'); ?>/flash/preview.jpg" 
                />
            </object>
        </span>
    </div><!--end .box-big-->	 
    <div class="box top left">
    	<h2>Check Cashing</h2>
        <span class="icon clipboard home"></span>
        <p>
        	People choose to cash their checks at Dollar$mart because we offer a convenient, confidential, cost effective option.
			Dollar$mart employees are committed to cashing every good check that is presented to us.
            <br />
			<a class="button" href="<?php bloginfo('url'); ?>/services-check-cashing-payday-loans/">Learn More</a>
        </p>
    </div><!--end .box-->

    <div class="box left">    	
    	<h2>Payday Loans</h2>        
    	<span class="icon money home"></span>
    	<table cellspacing="0" cellpadding="0">
    		<tr>
    			<th>Loan Amount</th>
    			<th>Fees as a dollar amount</th>
    			<th>Fees as an APR</th>
    		</tr>
            <tr>
                <td>$50</td>
                <td>$8.83</td>
                <td>460.42%</td>
            </tr>
            <tr>
                <td>$100</td>
                <td>$17.65</td>
                <td>460.16%</td>
            </tr>
                <tr>
                    <td>$150</td>
                <td>$26.47</td>
                <td>460.07%</td>
            </tr>
                <tr>
                    <td>$200</td>
                <td>$35.30</td>
                <td>460.16%</td>
            </tr>
                <tr>
                    <td>$255</td>
                <td>$45.00</td>
                <td>460.08%</td>
            </tr>
        </table>
        Fees / APR calculated based on a typical 14-day term   
    </div><!--end .box-->
		
    <div class="box left gold">      
        <h2>We Buy Gold<span>*</span></h2>        
        <span class="icon coin home"></span>
        <p>We pay cash for your gold. Contact us today! <br />
            <a class="button" href="<?php bloginfo('url'); ?>/services-check-cashing-payday-loans#gold">Learn More</a>
        </p>
    </div><!--end .box--> 

    <div class="box left giftcard">      
        <h2>We Buy Giftcards<span>*</span></h2>        
        <span class="icon giftcard home"></span>
        <p>We pay cash for your giftcards. Contact us today! <br />
            <a class="button" href="<?php bloginfo('url'); ?>/services-check-cashing-payday-loans#giftcard">Learn More</a>
        </p>
    </div><!--end .box--> 
		
    <div class="box-big right">
        <span class="center centertxt">
            <br />
			<object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="player" width="400" height="315">
                <param name="movie" value="<?php bloginfo('template_directory'); ?>/flash/player.swf" />
                <param name="allowfullscreen" value="true" />
                <param name="allowscriptaccess" value="always" />
                <param name="flashvars" value="file=<?php bloginfo('template_directory'); ?>/flash/Dollar_Smart_Works_for_Me.flv&image=<?php bloginfo('template_directory'); ?>/flash/preview2.jpg" />
                <embed type="application/x-shockwave-flash" id="player2" name="player2" src="<?php bloginfo('template_directory'); ?>/flash/player.swf" width="400" height="315" allowscriptaccess="always" allowfullscreen="true" flashvars="file=<?php bloginfo('template_directory'); ?>/flash/Dollar_Smart_Works_for_Me.flv&image=<?php bloginfo('template_directory'); ?>/flash/preview2.jpg" />
            </object>
        </span>
    </div> <!-- end box-big.right -->
    <div class="left asterix">
        <p><strong>*Not available in all locations</strong><br />
        DollarSmart is licensed by the department of Justice<br />
        Second Hand Dealer Permits 37051145, 371111994, 56041082, 56091066, 56081062, 56041081
        </p>

    </div>
  
</a>
    
</div><!--end #content-->    
<?php get_footer(); ?>