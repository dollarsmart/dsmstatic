<?php
/*
Template Name: Search Page
*/
?>

<?php get_header(); ?>

	<div id="content" class="narrowcolumn">

	<?php if (have_posts()) : ?>

		<h1>Search Results</h1>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post">
				<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h3>
				<div class="postmetadata">
					<p class="postmetacomment"><?php the_time('F jS, Y') ?></p>
					<p class="metadata">Posted in <?php the_category(', ') ?> <?php edit_post_link('Edit', '', ' | '); ?> by <?php the_author() ?> | <?php comments_popup_link('<span class="commentcount">No</span> Comments', '<span class="commentcount">1</span> Comment', '<span class="commentcount">%</span> Comments'); ?> </p>
				</div>
				
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div><?php next_posts_link('&laquo; Previous Entries') ?></div>
			<div><?php previous_posts_link('Next Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Whoops! Nothing here by that name. </h2>

	<?php endif; ?>

	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>