<?php include(TEMPLATEPATH . '/blog_sidebar.php'); ?>
<?php get_header(); ?>
<div class="post-header-wrapper">

    <div class="date-box">
        <span class="month"><?php the_time('j M') ?></span>
        <span class="year"><?php the_time('Y') ?></span>
    </div><!--end .date-box-->
    
    <div class="post-header">
	    <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>      
        <div class="postmetadata">
      	  <span class="metadata">By <?php the_author() ?></span>
        </div><!--end .postmetadata-->    
    </div><!--end .post-header-->

</div><!--end .post-header-wrapper-->

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post" id="post-<?php the_ID(); ?>">
				<h1>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?>			
                </a>
				</h1>
				<div class="postmetadata">
					<p class="postmetacomment"><?php the_time('F jS, Y') ?></p>
					<p class="metadata">Posted in <?php the_category(', ') ?> <?php edit_post_link('Edit', '', ' | '); ?> by <?php 
					the_author() ?> | <?php comments_popup_link('<span class="commentcount">No</span> Comments', '<span 
					class="commentcount">1</span> Comment', '<span class="commentcount">%</span> Comments'); ?> </p>
				</div>

				<div class="entry">
					<?php the_content('<br />Read the rest of this entry &raquo;'); ?>
				</div>
			<br />

			</div>

	<?php endwhile; ?>

		<div class="navigation">
			<?php next_posts_link('&lt; Previous') ?>
			<?php previous_posts_link('Next &gt;') ?>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php include (TEMPLATEPATH . "/searchform.php"); ?>

	<?php endif; ?>
		<!-- Previous/Next page navigation -->
	    <div class="page-nav">
		    <div class="nav-previous"><?php previous_posts_link('&laquo; Previous Page') ?></div>
	    	<div class="nav-next"><?php next_posts_link('Next Page &raquo;') ?></div>
	    </div>