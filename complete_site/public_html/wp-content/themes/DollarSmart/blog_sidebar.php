<div id="blog-sidebar">
	<img alt="" class="zero" src="<?php bloginfo('template_directory'); ?>/images/sidebar_top.jpg" />
    <span class="heading">
        <h2 class="categories">Categories</h2>
        <span class="icon money blog"></span>
    </span><!--end .heading-->
    <span class="shadow"></span>
    <ul>
    	<?php wp_list_categories('orderby=name&show_count=0&title_li=&exclude=50'); ?>
    </ul>
    
    <br />
    
    <span class="heading">
        <h2 class="recent">Recent Articles</h2>
        <span class="icon clipboard blog"></span>
    </span><!--end .heading-->
    <span class="shadow"></span>
    <ul>
    	<?php wp_get_archives('type=monthly&show_post_count=0'); ?>
    </ul>
    
    <br />
    
    <span class="heading">
        <h2 class="tags">Tags</h2>
        <span class="icon new blog"></span>
    </span><!--end .heading-->
    <span class="shadow"></span>
    <?php wp_tag_cloud(''); ?>
    
    <br />
    
    <div class="follow">
    	<a class="facebook" href="http://www.facebook.com" target="_blank">Facebook</a>
        <a class="twitter" href="http://www.twitter.com" target="_blank">Twitter</a>
    </div><!--end .follow-->
    
	<img alt="" class="zero" src="<?php bloginfo('template_directory'); ?>/images/sidebar_bottom.jpg" />
</div><!--end #blog-sidebar-->