/*****************************************************************

  AbsoluteFudge module
  ====================

  Usage:
  ------
  
  absolutefudge.js is only applicable where the browser is IE6. So
  the file should be loaded from within a conditional comment. Add this to the
  head section of the HTML:
  
  <!--[if lt IE 7]>
  <script type="text/javascript" src="absolutefudge.js"> </script>
  <script type="text/javascript">

    function onLoad() {
      if (AbsoluteFudge != undefined) {
        AbsoluteFudge.apply(document.getElementsByTagName('body')[0], true, true);
      }
      // other IE6 loading stuff...
    }

  </script>
  <![endif]-->

  and insert this bit below the body element:
  
<!--[if lt IE 7]>
<script type="text/javascript">document.body.onload = onLoad;</script>
<![endif]-->


  The apply() method walks the entire document. If you set the second and
  third parameters to true. The second parameter instructs apply() to walk the
  entire document looking for absolutely positioned elements. The third
  tells it to carry on through non absolutely positioned elements. If both
  are true, the entire document is traversed. If both are false it will just
  apply the fudge to that single element. If true,false then recursion
  is limited to absolutely positioned elements. searching stops once
  non-absolute elements are found.
  
  If you are generating html elements dynamically (e.g. Javascript/Ajax
  programming) then you may have to explicitly call apply() for the
  new element(s).


******************************************************************/

var AbsoluteFudge = new Object();

  AbsoluteFudge.isAbsolute = function (element) {
    return (element.currentStyle.position == 'absolute');
  }

  AbsoluteFudge.set = function(element) {
    if (element) {
      element.style.setExpression("height", "AbsoluteFudge.fudgeHeight(this)");
      element.style.setExpression("width", "AbsoluteFudge.fudgeWidth(this)");
    }
  }

  AbsoluteFudge.apply = function(element, recurse, full) {
    for (var i = 0; i < element.childNodes.length; i++) {
      var child = element.childNodes[i];
      if (child.nodeType == 1) {
        if (this.isAbsolute(child)) {
          this.set(child);
          if (recurse) {
            this.apply(child, recurse, full);
          }
        }
        else {
          if (full && recurse) {
            this.apply(child, recurse, full);
          }
        }
      }
    }
  }

  AbsoluteFudge.fudgeWidth = function(el) {
  if (el.currentStyle.right != 'auto') {
    try {
      var parent = el;
      var w = 0;
      do {
        parent = parent.parentNode;
        w = parent.clientWidth;
      }
      while (w == 0);
      if (document.documentElement.clientHeight != 0) {
        // in standards mode so width that gets set is inside padding/margin
        w = w - parseInt(el.currentStyle.paddingLeft)
              - parseInt(el.currentStyle.paddingRight);

        if (el.currentStyle.borderLeftStyle != 'none') {
          try {
            w = w - parseInt(el.currentStyle.borderLeftWidth);
          }
          catch(e) {
            w = w - 2; //estimated
          }
        }
        if (el.currentStyle.borderRightStyle != 'none') {
          try {
            w = w - parseInt(el.currentStyle.borderRightWidth);
          }
          catch(e) {
            w = w -2; //estimated
          }
        }
      }
      el.style.width = (w - parseInt(el.currentStyle.left) - parseInt(el.currentStyle.right)) + 'px';
    }
    catch(e) { }
  }
}

  AbsoluteFudge.fudgeHeight = function(el) {
  if (el.currentStyle.bottom != 'auto') {
    try {
      var parent = el;
      var h = 0;
      do {
        parent = parent.parentNode;
        h = parent.clientHeight;
      }
      while (h == 0);
      if (document.documentElement.clientHeight != 0) {
        //standards mode
        h = h - parseInt(el.currentStyle.paddingTop)
              - parseInt(el.currentStyle.paddingBottom);
        if (el.currentStyle.borderTopStyle != 'none') {
          try {
            h = h - parseInt(el.currentStyle.borderTopWidth);
          }
          catch(e) {
            h = h - 2;
          }
        }
        if (el.currentStyle.borderBottomStyle != 'none') {
          try {
            h = h - parseInt(el.currentStyle.borderBottomWidth);
          }
          catch(e) {
            h = h - 2;
          }
        }
      }
      var x = (h - parseInt(el.currentStyle.top) - parseInt(el.currentStyle.bottom)) + 'px';
      el.style.height = x;
    }
    catch(e) {}
  }
}
