<?php get_header(); ?>
<h1 id="page-title" class="img news">News</h1>
<div id="content" class="blog">
	<?php include(TEMPLATEPATH . '/blog_sidebar.php'); ?>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="post-header-wrapper">
    
        <div class="date-box">
            <span class="month"><?php the_time('j M') ?></span>
            <span class="year"><?php the_time('Y') ?></span>
        </div><!--end .date-box-->
        
        <div class="post-header">
            <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>      
            <div class="postmetadata">
              <span class="metadata">By <?php the_author() ?></span>
            </div><!--end .postmetadata-->    
        </div><!--end .post-header-->
    
    </div><!--end .post-header-wrapper-->
    
    <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
            <?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
            <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
            <p class="postmetadata2">
                This entry was posted on <?php the_time('l, F jS, Y') ?> at <?php the_time() ?> and is filed under <?php the_category(', ') ?>.
              You can follow any responses to this entry through the <?php comments_rss_link('RSS 2.0'); ?> feed.
              <?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                                // Both Comments and Pings are open ?>
              You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(true); ?>" rel="trackback">trackback</a> from your own site.
              <?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
                                // Only Pings are Open ?>
              Responses are currently closed, but you can <a href="<?php trackback_url(true); ?> " rel="trackback">trackback</a> from your own site.
              <?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                                // Comments are open, Pings are not ?>
              You can skip to the end and leave a response. Pinging is currently not allowed.
              <?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
                                // Neither Comments, nor Pings are open ?>
              Both comments and pings are currently closed.
              <?php } edit_post_link('Edit this entry.','',''); ?>
            </p><!--end .postmetadata2-->
        </div><!--end .entry-->
    </div><!--end .post-->

  <div class="clear"></div>
  <?php comments_template(); ?>
  
  <?php endwhile; else: ?>
  	<p>Sorry, no posts matched your criteria.</p>
  <?php endif; ?>
</div><!--end #content-->
<?php get_footer(); ?>