<!--Information for Store Locations-->
<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_hillcrest.jpg" />
    </span>
    <h2>San Diego Dollar Smart <small><em>in Hillcrest</em></small></h2>
    <p>
        405 Washington Street
        <br />
        San Diego, 92103
        <br />
        <strong><i>(619) 542-0500</i></strong>
						  
	</p>
    <p>
        Open Monday - Saturday<strong><i> 6:30am - 12am</i></strong>
        <br />
        Sunday <i><strong> 9am - 5pm</strong></i>
    </p>
    <a class="button" href="https://www.google.com/maps/dir//Dollar+Smart+Money+Center,+405+Washington+St,+San+Diego,+CA+92103,+United+States/@32.749733,-117.16146,17z/data=!4m12!1m3!3m2!1s0x0:0x59749950141b2f9b!2sDollar+Smart+Money+Center!4m7!1m0!1m5!1m1!1s0x80d954d154a54275:0x59749950141b2f9b!2m2!1d-117.16146!2d32.749733">Get Directions</a>
						<p><a class="button" href="https://plus.google.com/100776591048541794567/about">Google+ Page</a></p>
</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_chula.jpg" />
    </span>
    <h2>CHULA VISTA Dollar Smart</h2>
    <p>
        459 Broadway
        <br />
        Chula Vista, 91910
        <br />
        <strong><i>(619) 409-9916</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>7am - 11pm</i></strong>
        <br />
        Sunday <strong><i>9am - 5pm</i></strong><br><b>*Gift Cards and Gold Services not available at our Chula Vista branch.</b>

    </p>
    <a class="button" href="https://www.google.com.mx/maps/dir//Dollar+Smart+Money+Center,+459+Broadway,+Chula+Vista,+CA+91910,+United+States/@32.632402,-117.089744,17z/data=!4m12!1m3!3m2!1s0x0:0x32cb9356247e2834!2sDollar+Smart+Money+Center!4m7!1m0!1m5!1m1!1s0x80d94de5e6c0ee9d:0x32cb9356247e2834!2m2!1d-117.089744!2d32.632402">Get Directions</a>
	<p><a class="button" href="https://plus.google.com/116811819026170615446/about">Google+ Page</a></p>
</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_cajon.jpg" />
    </span>
    <h2>Dollar Smart EL CAJON</h2>
    <p>
        1103 Broadway
        <br />
        El Cajon, 92021
        <br />
        <strong><i>(619) 401-1667</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>6:30am - 9pm</i></strong>
        <br />
        Sunday <strong><i>9am - 5pm</i></strong>
    </p>
    <a class="button" href="https://www.google.com/maps/dir//Dollar+Smart+Money+Center,+1103+Broadway,+El+Cajon,+CA+92021,+United+States/@32.8068813,-116.946234,17z/data=!4m12!1m3!3m2!1s0x80d958535789f2e9:0x9ecc6c178a4d379a!2sDollar+Smart+Money+Center!4m7!1m0!1m5!1m1!1s0x80d958535789f2e9:0x9ecc6c178a4d379a!2m2!1d-116.944392!2d32.80719">Get Directions</a>
<p><a class="button" href="https://plus.google.com/114604793231152637324">Google+ Page</a></p>
		</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_doris.jpg" />
    </span>
    <h2>DORIS</h2>
    <p>
        1460 Doris Ave
        <br />
        Oxnard Ca 93033
        <br />
        <strong><i>(805) 487-1553</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>7am - 9pm</i></strong>
        <br />
        Sunday <strong><i> 9:30 am - 5:30 pm</i></strong>
    </p>
    <a class="button" href="http://www.mapquest.com/maps?city=Oxnard&state=CA&address=1460+Doris+Ave&zipcode=93030-8771&country=US&latitude=34.208318&longitude=-119.193969&geocode=ADDRESS">Get Directions</a>
</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_island.jpg" />
    </span>
    <h2>Channel Island</h2>
    <p>
        1420 Channel Islands blvd
        <br />
        Oxnard CA 93030
        <br />
        <strong><i>(805) 247-0616</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>6:30AM - 9:30PM</i></strong><br>
        Sunday <strong><i>9am -5:00pm</i></strong>
    </p>
    <a class="button" href="http://www.mapquest.com/maps?city=Oxnard&state=CA&address=1420+W+Channel+Islands+Blvd&zipcode=93033-4202&country=US&latitude=34.17566&longitude=-119.193818&geocode=ADDRESS">Get Directions</a>
</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_simi.jpg" />
    </span>
    <h2>Simi Valley</h2>
    <p>
        3998 Cochran St. #3
        <br />
        Simi Valley CA 93065
        <br />
        <strong><i>(805) 581-3799</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>7AM - 9PM</i></strong><br>
        Sunday <strong><i>10AM - 6PM</i></strong>
    </p>
    <a class="button" href="http://www.mapquest.com/maps?city=Simi+Valley&state=CA&address=3998+Cochran+St+Ste+3&zipcode=93063-2377&country=US&latitude=34.27907&longitude=-118.717882&geocode=ADDRESS">Get Directions</a>
</div><!--end .store-->

<div class="store">
	<span class="image left">
		<img alt="location" class="" src="<?php bloginfo('template_directory'); ?>/images/location_ventura.jpg" />
    </span>
    <h2>Ventura</h2>
    <p>
        3530 E Main St.
        <br />
        Ventura CA 93003
        <br />
        <strong><i>(805) 658-2373</i></strong>
	</p>
    <p>
        Monday - Saturday <strong><i>7:00AM - 9PM</i></strong><br>
        Sunday <strong><i>10AM - 6PM</i></strong>
    </p>
    <a class="button" href="http://www.mapquest.com/maps?city=Ventura&state=CA&address=3530+E+Main+St&zipcode=93003-5063&cat=Dollar+Smart&country=US&latitude=34.264814&longitude=-119.245779&geocode=ADDRESS">Get Directions</a>
</div><!--end .store-->