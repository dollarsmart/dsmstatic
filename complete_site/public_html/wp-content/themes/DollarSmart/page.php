<?php
get_header(); //Gets the Header
include(TEMPLATEPATH . '/variables.php'); //Page Variables
?>
<h1 id="page-title" <?php
	if (is_page($page_services)) { echo 'class="img services"'; }
	if (is_page($page_locations)) { echo 'class="img locations"'; }
	if (is_page($page_news)) { echo 'class="img news"'; }
	if (is_page($page_contact)) { echo 'class="img contact"'; }
?>>
	<?php the_title(); ?>
</h1>
<?php //Excludes normal sidebar from the Blog
	if (!is_page($page_news)) { get_sidebar(); }
?>
	<div id="content" <?php if (is_page($page_news)) {?> class="blog"<?php } ?>>
    
    	<?php //custom page content
        	if (is_page($page_locations)) {
				@include(TEMPLATEPATH . '/locations.php');
			}
			elseif (is_page($page_news)) {
				@include(TEMPLATEPATH . '/blog.php');
			}
		?>
        
	    <?php
			if (!is_page($page_news)) { //excludes the regular loop from the Blog
				if (have_posts()) : while (have_posts()) : the_post();
					//Checks for image for the top of the page
					$values = get_post_custom_values("image");
					if (isset($values[0])) { ?>
						<div id="page-image">
							<img alt="" class="" src="<?php  echo $values[0]; ?>" />
						</div><!--end #page-image-->
				<?php } ?>
                
				<div class="post" id="post-<?php the_ID(); ?>">
                    <div class="entry">
                        <?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
                        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
                    </div><!--end .entry-->
			
					<?php if(is_page($page_services)) { ?>
                        <div class="box services right">
                            <h2>Payday Loans</h2>
                            <span class="icon money home"></span>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th>Loan Amount</th>
                                    <th>Fees as a dollar amount</th>
                                    <th>Fees as an APR</th>
                                </tr>
                                <tr>
                                    <td>$50</td>
                                    <td>$8.83</td>
                                    <td>460.42%</td>
                                </tr>			
                                <tr>
                                    <td>$100</td>
                                    <td>$17.65</td>
                                    <td>460.16%</td>
                                </tr>			
                                <tr>
                                    <td>$150</td>
                                    <td>$26.47</td>
                                    <td>460.07%</td>
                                </tr>
                                <tr>
                                    <td>$200</td>
                                    <td>$35.30</td>
                                    <td>460.16%</td>
                                </tr>
                                <tr>
                                    <td>$255</td>
                                    <td>$45.00</td>
                                    <td>460.08%</td>
                                </tr>
                            </table>
                            Fees / APR calculated based on a typical 14-day term
                        </div><!--end .box-->
                    <?php } //end IF is_page for Services
                    
                    elseif (is_page($page_contact)) { ?>
                        <div class="box locations right">
                            <h2>Our Locations</h2>
                            <span class="icon storefront"></span>
                            <ul>
                                <li>Hillcrest</li>
                                <li>Chula Vista</li>
                                <li>El Cajon</li>
                                <li>Doris</li>
                                <li>Channel Island</li>
                                <li>Simi Valley</li>
                                <li>Ventura</li>
                            </ul>
                            <a class="button" href="<?php bloginfo('url'); ?>/locations">Get Directions</a>
                        </div><!--end .box-->
                    <?php } //end IF is_page for Contact
                        elseif (is_page($page_locations)) { ?>
                            <a id="top" class="right" href="#top">Top</a>
                	<?php }  //end IF is_page for Locations ?>    
				</div><!--end .post-->
		<?php endwhile; endif; ?>
       	<?php } //end if NOT blog IF statement ?>
	</div><!--end #content-->
<?php get_footer(); ?>