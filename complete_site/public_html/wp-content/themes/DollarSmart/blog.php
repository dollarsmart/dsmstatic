<?php include(TEMPLATEPATH . '/blog_sidebar.php'); ?>
<?php
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	$wp_query->query('showposts=10'.'&paged='.$paged);
	$wp_query->query('cat=-50');
	while ($wp_query->have_posts()) : $wp_query->the_post();
?>
<div class="post-header-wrapper">

    <div class="date-box">
        <span class="month"><?php the_time('j M') ?></span>
        <span class="year"><?php the_time('Y') ?></span>
    </div><!--end .date-box-->
    
    <div class="post-header">
	    <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>      
        <div class="postmetadata">
      	  <span class="metadata">By <?php the_author() ?></span>
        </div><!--end .postmetadata-->    
    </div><!--end .post-header-->

</div><!--end .post-header-wrapper-->

<div class="post" id="post-<?php the_ID(); ?>">
    
    <div class="entry">
        <?php the_excerpt(); ?>
    </div><!--end .entry-->

    <span class="readmore">
        <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">Read More &raquo;</a>
    </span><!--end .readmore-->

</div><!--end .post-->
<?php endwhile; ?>

<div class="navigation">
	<?php next_posts_link('&lt; Previous') ?>
    <?php previous_posts_link('Next &gt;') ?>
</div><!--end .navigation-->

<?php $wp_query = null; $wp_query = $temp;?>